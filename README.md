# Getting Started

Install the packages
```
npm install
```

Run Kafka Instance with docker
```
docker-compose up
```

Run cron job (producer)
```
npm run start:producer
```

Run consumer app
```
npm run start:consumer
```