const { Kafka } = require('kafkajs');
const mongoose = require('mongoose');
const { fetchStates } = require('./limsApi');
const { State } = require('./models');


const kafka = new Kafka({
  clientId: "ecompass-consumer",
  brokers: ["localhost:9092"]
})

const topic = "sync-master"
const consumer = kafka.consumer({ groupId: 'test-group' })

mongoose.connect('mongodb://localhost:27017/ecompass_demo');

const getQueries = (currentData, incomingData) => {
  const queries = [];

  // currentData.forEach(currentRecord => {
  //   const incomingRecord = incomingData.find(item => item.StateId === currentRecord.StateId);
  //   if (!incomingRecord) {
  //     queries.push({ name: "delete", record: currentRecord});
  //   }
  // })

  incomingData.forEach(incomingRecord => {
    const currentRecord = currentData.find(item => item.StateId === incomingRecord.StateId);
    if (currentRecord) {
      if (currentRecord.ModifiedDate !== incomingRecord.ModifiedDate) {
        queries.push({ name: "update", record: incomingRecord });
      }
    } else {
      queries.push({ name: "insert", record: incomingRecord })
    }
  })

  return queries;
}

const syncMaster = async () => {
  try {
    const { data } = await fetchStates();
    const incomingData = data?.Success?.Data;
    const currentData = await State.find();

    const queries = getQueries(currentData, incomingData);
    if (queries.length > 0) {
      const insertItems = queries.filter(query => query.name === "insert");
      const updateItems = queries.filter(query => query.name === "update");

      if (insertItems.length > 0) {
        const newRecords = insertItems.map(item => item.record);
        console.log("=======Inserting new Records=======");
        console.log(newRecords);
        State.insertMany(newRecords)
          .then(() => console.log('✅Inserted New States'))
          .catch(err => "❌Could not Insert into State Master")
      }

      if (updateItems.length > 0) {
        updateItems.forEach(item => {
          State.replaceOne({ StateId: item.record.StateId }, item.record)
          .then(() => console.log("Updated State", item.record))
          .catch(err => "Could not Update State Master")
        })
      }
    } else {
      console.log("Already synced up👍");
    }
  } catch (err) {
    console.error(err);
  }
}

// Main Function

const run = async () => {
  await consumer.connect();
  await consumer.subscribe({ topic, fromBeginning: true });

  await consumer.run({
    eachMessage: async ({ topic, partition, message }) => {
      syncMaster();
    },
  })
}

run().catch(e => console.error(`[example/consumer] ${e.message}`, e))

// Handle Consumer Disconnection

const errorTypes = ['unhandledRejection', 'uncaughtException']
const signalTraps = ['SIGTERM', 'SIGINT', 'SIGUSR2']

errorTypes.forEach(type => {
  process.on(type, async e => {
    try {
      console.log(`process.on ${type}`)
      console.error(e)
      await consumer.disconnect()
      process.exit(0)
    } catch (_) {
      process.exit(1)
    }
  })
})

signalTraps.forEach(type => {
  process.once(type, async () => {
    try {
      await consumer.disconnect()
    } finally {
      process.kill(process.pid, type)
    }
  })
})
