const axiosInstance = require("./axiosInstance");

const fetchStates = async () => {
    return axiosInstance.post("/GetThirdPartyStateList")
}

const fetchOrgans = async () => {
    return axiosInstance.post("/GetThirdPartyOrganList")
}

module.exports = {
    fetchStates,
    fetchOrgans,
}