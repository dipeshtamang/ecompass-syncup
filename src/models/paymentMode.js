const mongoose = require("mongoose");

const paymentModeSchema = new mongoose.Schema({
    name: String,
    POSDevicePaymentCode: String,
    createdAt: Date,
    updatedAt: Date
})

module.exports = mongoose.model('PaymentMode', paymentModeSchema)