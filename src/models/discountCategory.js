const mongoose = require("mongoose");

const discountCategorySchema = new mongoose.Schema({
    name: String,
    percentage: mongoose.Types.Decimal128,
    maxDiscountAmount: mongoose.Types.Decimal128,
    integrationDiscountCode: String,
    createdAt: Date,
    updatedAt: Date,
})

module.exports = mongoose.model('DiscountCategory', discountCategorySchema)