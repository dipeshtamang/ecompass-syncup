const mongoose = require("mongoose");

const diseaseSchema = new mongoose.Schema({
    name: String,
    SNOMED: String,
    ICDCode: String,
    type: String,
    Attachment: String,
    contentType: String,
    createdAt: Date,
    updatedAt: Date,
})

module.exports = mongoose.model('Disease', diseaseSchema)