const mongoose = require("mongoose");

const citySchema = new mongoose.Schema({
    city: String,
    stateId: mongoose.Schema.Types.ObjectId,
    createdAt: Date,
    updatedAt: Date,
})

module.exports = mongoose.model('City', citySchema)