const mongoose = require("mongoose");

const branchSchema = new mongoose.Schema({
    name: String,
    code: String,
    address: String,
    contact: String,
    email: String,
    isProcessingLocation: Boolean,
    integrationBranchCode: String,
    latitude: mongoose.Schema.Types.Decimal128,
    longitude: mongoose.Schema.Types.Decimal128,
    cityId: mongoose.Schema.Types.ObjectId,
}, {
    timestamps: true
})

module.exports = mongoose.model('Branch', branchSchema)