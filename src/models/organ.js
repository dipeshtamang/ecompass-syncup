const mongoose = require("mongoose");

const organSchema = new mongoose.Schema({
    name: String,
    image: String,
    createdAt: Date,
    updatedAt: Date,
})

module.exports = mongoose.model('Organ', organSchema)