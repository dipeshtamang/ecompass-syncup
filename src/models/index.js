const State = require("./state");
const Organ = require("./organ");
const Disease = require("./disease");
const PaymentMode = require("./paymentMode");

module.exports = {
    State,
    Organ,
    Disease,
    PaymentMode
}