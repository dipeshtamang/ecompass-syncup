const mongoose = require("mongoose");

const stateSchema = new mongoose.Schema({
    StateId: Number,
    StateName: String,
    CreatedDate: String,
    ModifiedDate: String,
})

module.exports = mongoose.model('State', stateSchema)