const { Kafka } = require('kafkajs');
const cron = require("node-cron");

const kafka = new Kafka({
    clientId: "ecompass-cron",
    brokers: ["localhost:9092"]
})

const topic = "sync-master"
const producer = kafka.producer()

const sendMessage = () => {
  return producer.send({
    topic,
    messages: [
      { value: 'state' },
    ],
  })
}

// Main Function

const run = async () => {
  await producer.connect();

  cron.schedule("*/10 * * * * *", () => {
    console.log("Sending message...")
    sendMessage();
  });
}

run()
.catch(err => console.error(`[example/producer] ${e.message}`, e))

// Handle Producer Disconnection

const errorTypes = ['unhandledRejection', 'uncaughtException']
const signalTraps = ['SIGTERM', 'SIGINT', 'SIGUSR2']

errorTypes.forEach(type => {
  process.on(type, async () => {
    try {
      console.log(`process.on ${type}`)
      await producer.disconnect()
      process.exit(0)
    } catch (_) {
      process.exit(1)
    }
  })
})

signalTraps.forEach(type => {
  process.once(type, async () => {
    try {
      await producer.disconnect()
    } finally {
      process.kill(process.pid, type)
    }
  })
})